\label{eq:ts_mirp}
\begin{eqnarray}
\slabel{eq:ts_obj}\max & & 
  \begin{split} & \sum_{i \in \J^{\rm C}}\sum_{t \in \T}\sum_{v \in \V}R_{it}f^v_{it} 
- \sum_{v \in \V}\sum_{i \in \J\cup\{o(v)\}} \sum_{j\in\J\cup\{d(v)\}} \sum_{t\in\T}C_{ijv}x_{ijvt} \\
 & - \sum_{i \in \J}\sum_{t \in \T}\sum_{v \in \V}(t\epsilon_z)o_{ivt}
- \sum_{i \in \J}\sum_{t \in \T}P_{it}\alpha_{it}      
  \end{split} \\
\slabel{eq:ts_initialPort} \mathrm{s. t.} \ & &  x_{o(v)j^0_vv0} = 1, \enspace \forall v \in \V \\
\slabel{eq:ts_sinkFlow}  	& &  \sum_{i\in\J}\sum_{t\in\T}x_{id(v)vt} = 1, \enspace \forall v \in \V \\
\slabel{eq:ts_flowBalance}	& & \begin{split} 
				      \sum_{j\in\J\cup\{o(v)\}}& x_{jiv,t-T_{jiv}} +  w_{iv,t-1} = \\ 
                          	      & \sum_{j\in\J\cup\{d(v)\}}x_{ijvt} + w_{ivt}, \enspace \forall i \in \J, t \in \T, v \in \V \\
                          	    \end{split} \\                         	    
\slabel{eq:ts_inventoryPort}	& &  s_{it} = s_{i,t-1} + {\rm\Delta}_i \left(D_{it} - \sum_{v \in \V}f^v_{it} - \alpha_{it}\right), \enspace  \forall \ i \in \J, t \in \T \\
\slabel{eq:ts_initPort}		& & s_{i,0} = S^0_i, \enspace \forall i \in \J \\
\slabel{eq:ts_inventoryVessel}  & & s^v_t = s^v_{t-1} + \sum_{i\in\J} {\rm\Delta}_if^v_{it}, \enspace  \forall \ t \in \T, v \in \V \\
\slabel{eq:ts_initVessel}	& & s^v_1 = L^0_v, \enspace \forall v \in \V \\
\slabel{eq:ts_berthLimit}	& & \sum_{v \in \V} o_{ivt} \leq B_i \enspace  \forall \ i \in \J, t \in \T \\
\slabel{eq:ts_workOnNode}	& & o_{ivt} \leq \sum_{j \in \J}x_{ijvt}, \enspace \forall \ i \in \J, t \in \T, v \in \V \\
\slabel{eq:ts_atCapacity} 	& & s^v_t \geq Q^vx_{ijvt} , \enspace \forall \ v \in \V, i \in \J^{\rm P}, j \in \J^{\rm C}\cup\{d(v)\} , t \in \T \\
\slabel{eq:ts_empty} 		& & s^v_t \leq Q^v(1-x_{ijvt}) , \enspace \forall \ v \in \V, i \in \J^{\rm C}, j \in \J^{\rm P}\cup\{d(v)\} , t \in \T \\
\slabel{eq:ts_maxCumAlpha}	& &  \sum_{t \in \T}\alpha_{it} \leq \alpha^{{\max}}_i , \enspace \forall \ i \in \J \\
\slabel{eq:ts_maxAlpha}		& & 0 \leq \alpha_{it} \leq \alpha^{{\max}}_{it}, \enspace  \forall \ i \in \J, t \in \T \\
\slabel{eq:ts_maxOper}		& & F^{\min}_{it}o_{ivt} \leq f^v_{it} \leq F^{\max}_{it}o_{ivt}, \enspace \forall \ i \in \J, t \in \T, v \in \V \\
\slabel{eq:ts_portCap}		& & S^{\min}_{i} \leq s_{it} \leq S^{\max}_{i}, \enspace \forall \ i \in \J, t \in \T \\
\slabel{eq:ts_vesselCap} 	& & 0 \leq s^v_t \leq Q^v, \enspace \forall \ t \in \T, v \in \V \\
\slabel{eq:ts_binX}		& & x_{ijvt} \in \{0,1\}, \enspace \forall \ i \in \J\cup\{o(v)\}, j \in \J \cup \{d(v)\}, t \in \T, v \in \V \\
\slabel{eq:ts_binO}		& & o_{ivt}, w_{ivt} \in \{0,1\}, \ \forall i \in \J, t \in \T, v \in \V
\end{eqnarray}